package com.simple.project1mutsasns.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;


@Getter
@AllArgsConstructor
@ToString
public class AppException extends RuntimeException{
    private ErrorCode errorCode;
    private String message;

}
