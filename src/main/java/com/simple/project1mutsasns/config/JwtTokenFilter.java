package com.simple.project1mutsasns.config;

import com.simple.project1mutsasns.domain.entity.User;
import com.simple.project1mutsasns.service.UserService;
import com.simple.project1mutsasns.utils.JwtTokenProvider;
import com.simple.project1mutsasns.utils.JwtTokenUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;

@RequiredArgsConstructor
@Slf4j
public class JwtTokenFilter extends OncePerRequestFilter {

//    private final UserService userService;
//    private final String secretKey;

    private final JwtTokenProvider tokenProvider;



    //실제 필터링 로직 : Jwt Token의 Authentication 정보를 현재 실행 중인 Security Context에 저장하기 위함
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException, ServletException, IOException {
        String jwt = resolveToken(request); // 요청 header에서 Token 꺼내줌


        if (StringUtils.hasText(jwt) && !tokenProvider.isExpired(jwt)) { //토큰이 존재하고, 만료되지 않았으면
            Authentication authentication = tokenProvider.getAuthentication(jwt); //tokenProvider를 통해 인증 객체가 생성되도록 구현
            SecurityContextHolder.getContext().setAuthentication(authentication); //인증 객체를 SecurityContextHolder에 저장
        }

        filterChain.doFilter(request,response);
    }

    //토큰 정보 가져오기
    public String resolveToken(HttpServletRequest request) {
        String header = request.getHeader(HttpHeaders.AUTHORIZATION);
        if (StringUtils.hasText(header) && header.startsWith("Bearer ")) {
            return header.substring(7);
        }
        return null;
    }


    //    @Override
//    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
//        //개찰구 역할
//        final String authorizationHeader = request.getHeader(HttpHeaders.AUTHORIZATION);
//
//        //언제 막아야하는지
//        //1.토큰이 없을 경우
//        if (authorizationHeader == null || !authorizationHeader.startsWith("Bearer ")){
//            filterChain.doFilter(request, response);
//            return;
//        }
//
//        //토큰 분리
//        String token;
//        try {
//            token = authorizationHeader.split(" ")[1];
//        }catch (Exception e){
//            filterChain.doFilter(request, response);
//            return;
//        }
//
//        //2.만료된 토큰을 가져올때 만료 확인
//        if(JwtTokenUtils.isExpired(token, secretKey)) {
//            filterChain.doFilter(request, response);
//            return;
//        }
//        //3.토큰이 유효하지 않을때
//        // Token- Claim에서 UserName꺼내기
//        // 들어갈땐 object 반환값은 STRING
//        String userName = JwtTokenUtils.getUserName(token, secretKey);
//
//        // username으로 User 찾아오기
//        User user = userService.getUserByUserName(userName);
//
//        // 권한을 부여  막기
//        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(user.getUserName(), null, List.of(new SimpleGrantedAuthority(user.getRole().name())));
//
//        authenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
//        SecurityContextHolder.getContext().setAuthentication(authenticationToken);
//        filterChain.doFilter(request, response);
//    }    private final JwtTokenProvider tokenProvider;
//
//
//    //실제 필터링 로직 : Jwt Token의 Authentication 정보를 현재 실행 중인 Security Context에 저장하기 위함
//    @Override
//    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException, ServletException, IOException {
//        String jwt = resolveToken(request); // 요청 header에서 Token 꺼내줌
//
//
//        if (StringUtils.hasText(jwt) && !tokenProvider.isExpired(jwt)) { //토큰이 존재하고, 만료되지 않았으면
//            Authentication authentication = tokenProvider.getAuthentication(jwt); //tokenProvider를 통해 인증 객체가 생성되도록 구현
//            SecurityContextHolder.getContext().setAuthentication(authentication); //인증 객체를 SecurityContextHolder에 저장
//        }
//
//        filterChain.doFilter(request,response);
//    }
//
//    //토큰 정보 가져오기
//    public String resolveToken(HttpServletRequest request) {
//        String header = request.getHeader(HttpHeaders.AUTHORIZATION);
//        if (StringUtils.hasText(header) && header.startsWith("Bearer ")) {
//            return header.substring(7);
//        }
//        return null;
//    }


}
