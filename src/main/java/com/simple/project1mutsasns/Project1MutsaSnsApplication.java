package com.simple.project1mutsasns;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Project1MutsaSnsApplication {

    public static void main(String[] args) {
        SpringApplication.run(Project1MutsaSnsApplication.class, args);
    }

}
