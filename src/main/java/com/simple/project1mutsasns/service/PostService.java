package com.simple.project1mutsasns.service;

import com.simple.project1mutsasns.domain.UserRole;
import com.simple.project1mutsasns.domain.dto.Response;
import com.simple.project1mutsasns.domain.dto.PostDto;
import com.simple.project1mutsasns.domain.dto.request.PostModifyRequest;
import com.simple.project1mutsasns.domain.entity.Post;
import com.simple.project1mutsasns.domain.entity.User;
import com.simple.project1mutsasns.exception.AppException;
import com.simple.project1mutsasns.exception.ErrorCode;
import com.simple.project1mutsasns.repository.PostRepository;
import com.simple.project1mutsasns.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;


import java.util.Objects;

@Service
@RequiredArgsConstructor
public class PostService {

    private final PostRepository postRepository;
    private final UserRepository userRepository;

    public PostDto create(Response.PostCreateRequest dto, String userName) {
        //userName 없을 때
//        User user = userRepository.findByUserName(userName)
//                .orElseThrow(() -> new AppException(ErrorCode.USERNAME_NOT_FOUND, ErrorCode.USERNAME_NOT_FOUND.getMessage()));


        User user = validateUser(userName);
        //저장
        Post savedPost = postRepository.save(Post.of(dto.getTitle(), dto.getBody(), user));

        return PostDto.builder()
                .id(savedPost.getId())
                .title(savedPost.getTitle())
                .body(savedPost.getBody())
                .build();
    }

    public Long delete(Long id, String userName) {
        validatePost(id, userName);

        //해당 post가 없을때
//                Post post = postRepository.findById(id).orElseThrow(() ->
//               new AppException(ErrorCode.POST_NOT_FOUND, ErrorCode.POST_NOT_FOUND.getMessage()));

        //userName이 없을때
//        User user = userRepository.findByUserName(userName)
//                .orElseThrow(() -> new AppException(ErrorCode.USERNAME_NOT_FOUND, ErrorCode.USERNAME_NOT_FOUND.getMessage()));

        //다른 userName이 접근할 때
//        if (!Objects.equals(post.getUser().getUserName(), userName)) {
//            throw new AppException(ErrorCode.INVALID_PERMISSION, ErrorCode.INVALID_PERMISSION.getMessage());
//        }
        try {
            postRepository.deleteById(id);
        } catch (AppException e) {
            e.printStackTrace();
        }
        return id;

    }

    public PostDto getOne(Long id) {
        Post post = postRepository.findById(id)
                .orElseThrow(() -> new AppException(ErrorCode.POST_NOT_FOUND, ErrorCode.POST_NOT_FOUND.getMessage()));
        return PostDto.fromPost(post);

    }

    public Page<PostDto> getAll(Pageable pageable) {
        Page<Post> postPage = postRepository.findAll(pageable);
        Page<PostDto> postDtoPage = postPage.map(post -> PostDto.fromPost(post));
        return postDtoPage;
    }


    public PostDto update(Long id, PostModifyRequest dto, String userName) {
        //해당 post가 없을때
        Post post = postRepository.findById(id).orElseThrow(() ->
                new AppException(ErrorCode.POST_NOT_FOUND, ErrorCode.POST_NOT_FOUND.getMessage()));

        //userName이 없을때
        User user = userRepository.findByUserName(userName)
                .orElseThrow(() -> new AppException(ErrorCode.USERNAME_NOT_FOUND, ErrorCode.USERNAME_NOT_FOUND.getMessage()));

        //다른 userName이 접근할 때
        if (!Objects.equals(post.getUser().getUserName(), userName)) {
            throw new AppException(ErrorCode.INVALID_PERMISSION, ErrorCode.INVALID_PERMISSION.getMessage());
        }

        post.setTitle(dto.getTitle());
        post.setBody(dto.getBody());
        Post savedPost = postRepository.save(post);
        return PostDto.fromPost(savedPost);
    }

    //반복되는 구문 메서드로 만들기
    private boolean validatePost(Long postId, String userName) {
        User findUser = userRepository.findByUserName(userName).orElseThrow(() ->
                new AppException(ErrorCode.USERNAME_NOT_FOUND, ErrorCode.USERNAME_NOT_FOUND.getMessage()));

        Post post = postRepository.findById(postId).orElseThrow(() ->
                new AppException(ErrorCode.POST_NOT_FOUND, ErrorCode.POST_NOT_FOUND.getMessage()));

        if (post.getUser().getId() == findUser.getId() || findUser.getRole() == UserRole.ADMIN) {
            return true;
        } else {
            throw new AppException(ErrorCode.INVALID_PERMISSION, ErrorCode.INVALID_PERMISSION.getMessage());
        }
    }

    //반복되는 구문들 메서드로 만들기
    private User validateUser(String userName) {
        return userRepository.findByUserName(userName).orElseThrow(() ->
                new AppException(ErrorCode.USERNAME_NOT_FOUND, ErrorCode.USERNAME_NOT_FOUND.getMessage()));
    }


}
