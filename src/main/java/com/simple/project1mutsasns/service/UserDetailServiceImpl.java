package com.simple.project1mutsasns.service;

import com.simple.project1mutsasns.domain.entity.User;
import com.simple.project1mutsasns.domain.entity.UserDetailImpl;
import com.simple.project1mutsasns.exception.AppException;
import com.simple.project1mutsasns.exception.ErrorCode;
import com.simple.project1mutsasns.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class UserDetailServiceImpl implements UserDetailsService {

    private final UserRepository userRepository;

    //유저네임 존재하는지 확인
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUserName(username).orElseThrow(() ->
                new AppException(ErrorCode.USERNAME_NOT_FOUND, ErrorCode.USERNAME_NOT_FOUND.getMessage()));
        if (user != null) {
            UserDetailImpl userDetails = new UserDetailImpl(user);
            return userDetails;
        }
        return null;
    }


}
