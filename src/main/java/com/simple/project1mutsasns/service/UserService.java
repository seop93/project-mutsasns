package com.simple.project1mutsasns.service;

import com.simple.project1mutsasns.domain.UserRole;
import com.simple.project1mutsasns.domain.dto.*;
import com.simple.project1mutsasns.domain.dto.request.UserChangeRequest;
import com.simple.project1mutsasns.domain.dto.request.UserJoinRequest;
import com.simple.project1mutsasns.domain.dto.request.UserLoginRequest;
import com.simple.project1mutsasns.domain.entity.User;
import com.simple.project1mutsasns.exception.AppException;
import com.simple.project1mutsasns.exception.ErrorCode;
import com.simple.project1mutsasns.repository.UserRepository;
import com.simple.project1mutsasns.utils.JwtTokenProvider;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserService {
    private final UserRepository userRepository;
    private final BCryptPasswordEncoder encoder;
    private final JwtTokenProvider tokenProvider;

    @Value("${jwt.token.secret}")
    private String key;
    private Long expireTimeMs = 1000 * 60 * 60l; // 1시간


    public UserDto join(UserJoinRequest dto) {

        //userName 중복체크
//        userRepository.findByUserName(dto.getUserName())
//                .ifPresent(user -> {
//                    throw new AppException(ErrorCode.DUPLICATED_USER_NAME,ErrorCode.DUPLICATED_USER_NAME.getMessage());
//                });

        DuplicateUser(dto.getUserName());

        //저장
        String securityPassword = encoder.encode(dto.getPassword());
        User savedUser = userRepository.save(dto.toEntity(securityPassword));

        return UserDto.builder()
                .id(savedUser.getId())
                .userName(savedUser.getUserName())
                .password(savedUser.getPassword())
                .role(savedUser.getRole())
                .build();
    }

    public String login(UserLoginRequest dto) {
//        //username 있는지 확인
//        User selectedUser = userRepository.findByUserName(dto.getUserName())
//                .orElseThrow(() -> new AppException(ErrorCode.USERNAME_NOT_FOUND, ErrorCode.USERNAME_NOT_FOUND.getMessage()));
//
        User selectedUser = findUserByUserName(dto.getUserName());
        //password 알맞게 입력 확인
        if (!encoder.matches(dto.getPassword(), selectedUser.getPassword())) {
            throw new AppException(ErrorCode.INVALID_PASSWORD, ErrorCode.INVALID_PASSWORD.getMessage());
        }

        //토큰 넘김
        return tokenProvider.createToken(dto.getUserName(), selectedUser.getRole().getValue());

    }

    public UserRole changeRole(Long id, UserChangeRequest request) {
        User findUser = userRepository.findById(id).orElseThrow(() ->
                new AppException(ErrorCode.USERNAME_NOT_FOUND, ErrorCode.USERNAME_NOT_FOUND.getMessage()));

        findUser.changeRole(request.getRole());

        return findUser.getRole();
    }







//    //필터체인으로 이름 가져오기
//    public User getUserByUserName(String userName) {
//        return userRepository.findByUserName(userName)
//                .orElseThrow(() -> new AppException(ErrorCode.USERNAME_NOT_FOUND, ErrorCode.USERNAME_NOT_FOUND.getMessage()));
//    }


    //유저네임이 중복확인
    private void DuplicateUser(String userName) {
        userRepository.findByUserName(userName).ifPresent(user -> {
            throw new AppException(ErrorCode.DUPLICATED_USER_NAME, ErrorCode.DUPLICATED_USER_NAME.getMessage());
        });
    }

    //유저네임이 유효한지
    public User findUserByUserName(String userName){
        return userRepository.findByUserName(userName).orElseThrow(() ->
                new AppException(ErrorCode.USERNAME_NOT_FOUND, ErrorCode.USERNAME_NOT_FOUND.getMessage()));
    }






}
