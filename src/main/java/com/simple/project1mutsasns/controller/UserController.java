package com.simple.project1mutsasns.controller;

import com.simple.project1mutsasns.domain.UserRole;
import com.simple.project1mutsasns.domain.dto.*;
import com.simple.project1mutsasns.domain.dto.request.UserChangeRequest;
import com.simple.project1mutsasns.domain.dto.request.UserJoinRequest;
import com.simple.project1mutsasns.domain.dto.request.UserLoginRequest;
import com.simple.project1mutsasns.domain.dto.response.UserChangeResponse;
import com.simple.project1mutsasns.domain.dto.response.UserJoinResponse;
import com.simple.project1mutsasns.domain.dto.response.UserLoginResponse;
import com.simple.project1mutsasns.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("api/v1/users")
public class UserController {
    private final UserService userService;


    @PostMapping("/join")
    public Response<UserJoinResponse> join(@RequestBody UserJoinRequest dto) {
        UserDto userDto = userService.join(dto);
        UserJoinResponse response = new UserJoinResponse(userDto.getId(), userDto.getUserName());
        return Response.success(response);
    }

    @PostMapping("/login")
    public Response<UserLoginResponse> login(@RequestBody UserLoginRequest dto) {
        String token = userService.login(dto);
        UserLoginResponse response = new UserLoginResponse(token);
        return Response.success(response);
    }

    @PostMapping("/{id}/role/change")
    public Response<UserChangeResponse> changeRole(@PathVariable Long id, @RequestBody UserChangeRequest request){
        UserRole modifiedUserRole = userService.changeRole(id, request);
        return Response.success(new UserChangeResponse("변경 되었습니다.", modifiedUserRole));
    }



}