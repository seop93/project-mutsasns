package com.simple.project1mutsasns.controller;

import com.simple.project1mutsasns.domain.dto.*;
import com.simple.project1mutsasns.domain.dto.request.PostModifyRequest;
import com.simple.project1mutsasns.domain.dto.response.PostCreateResponse;
import com.simple.project1mutsasns.domain.dto.response.PostDeleteResponse;
import com.simple.project1mutsasns.domain.dto.response.PostModifyResponse;
import com.simple.project1mutsasns.service.PostService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping("/api/v1/posts")
@RequiredArgsConstructor
public class PostController {

    private final PostService postService;

    @PostMapping("")
    public Response<PostCreateResponse> create(@RequestBody Response.PostCreateRequest dto, @ApiIgnore Authentication authentication) {
        PostDto postDto = postService.create(dto, authentication.getName());
        PostCreateResponse response = new PostCreateResponse("포스트 등록 완료", postDto.getId());
        return Response.success(response);
    }

    @DeleteMapping("/{id}")
    public Response<PostDeleteResponse> delete(@PathVariable Long id, Authentication authentication) {
        Long postId = postService.delete(id, authentication.getName());
        return Response.success(new PostDeleteResponse("포스트 삭제 완료", postId));
    }

    @GetMapping("/{id}")
    public Response<PostDto> getPost(@PathVariable Long id) {
        PostDto postDto = postService.getOne(id);
        return Response.success(postDto);
    }

    @GetMapping("")
    public Response<Page<PostDto>> getAll(Pageable pageable) {
        Page<PostDto> postDtoPage = postService.getAll(pageable);
        return Response.success(postDtoPage);
    }

    @PutMapping("/{id}")
    public Response<PostModifyResponse> update(@PathVariable Long id, @RequestBody PostModifyRequest dto, Authentication authentication) {
        PostDto postDto = postService.update(id, dto, authentication.getName());
        return Response.success(new PostModifyResponse("포스트 수정 성공", postDto.getId()));
    }


}
