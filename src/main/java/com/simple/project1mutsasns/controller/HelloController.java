package com.simple.project1mutsasns.controller;

import com.simple.project1mutsasns.domain.dto.Response;
import com.simple.project1mutsasns.service.AlgorithmService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("api/v1/hello")
@RestController
@RequiredArgsConstructor
public class HelloController {

    private final AlgorithmService algorithmService;

    @GetMapping("")
    public ResponseEntity<String> hello(){
        return ResponseEntity.ok().body("변흥섭");
    }

    @GetMapping("/{num}")
    public Integer sumOfDigits(@PathVariable Integer num){
        return algorithmService.sumOfDigits(num);
    }
}
