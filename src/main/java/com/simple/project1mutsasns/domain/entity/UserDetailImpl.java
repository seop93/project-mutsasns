package com.simple.project1mutsasns.domain.entity;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;

//유저의 정보를 가져오는 UserDetails 인터페이스를 상속하는 클래스이다.
//Authentication을 담고 , user.getRole().getValue()를 통해 사용자의 권한을 주고 가져올 수 있다.
//인증 권한을 위해 생성
@RequiredArgsConstructor
public class UserDetailImpl implements UserDetails {
    private final User user;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        Collection<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(() -> user.getRole().getValue());
        return authorities;
    }


    //비밀번호 확인
    @Override
    public String getPassword() {
        return user.getPassword();
    }

    //유저네임 확인
    @Override
    public String getUsername() {
        return user.getUserName();
    }

    //계정이 만료되있는지 확인
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    //계정이 잠겨있는지 확인
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    //비밀번호가 만료되어있는지 확인
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    //계정이 활성화 되어있는지 확인
    @Override
    public boolean isEnabled() {
        return true;
    }
}
