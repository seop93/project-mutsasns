package com.simple.project1mutsasns.domain.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class PostDeleteResponse {

    private String message;
    private Long id;

}
