package com.simple.project1mutsasns.domain.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class PostCreateResponse {

    private String message;
    private Long id;

}
