package com.simple.project1mutsasns.domain.dto.request;

import com.simple.project1mutsasns.domain.UserRole;
import com.simple.project1mutsasns.domain.entity.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@Getter
public class UserLoginRequest {
    private String userName;
    private String password;

    public User toEntity(String password) {
        return  User.builder()
                .userName(this.userName)
                .password(password)
                .role(UserRole.USER)
                .build();
    }


}
