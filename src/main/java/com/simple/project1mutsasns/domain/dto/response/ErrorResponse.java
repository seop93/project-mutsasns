package com.simple.project1mutsasns.domain.dto.response;

import com.simple.project1mutsasns.exception.ErrorCode;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter

public class ErrorResponse {
    private ErrorCode errorCode;
    private String message;

}
