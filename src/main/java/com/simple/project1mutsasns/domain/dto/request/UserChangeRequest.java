package com.simple.project1mutsasns.domain.dto.request;

import com.simple.project1mutsasns.domain.UserRole;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Getter
public class UserChangeRequest {
    private UserRole role;

}
