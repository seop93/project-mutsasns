package com.simple.project1mutsasns.domain.dto.response;

import com.simple.project1mutsasns.domain.UserRole;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class UserChangeResponse {
    private String message;
    private UserRole role;

}
