package com.simple.project1mutsasns.utils;


import com.simple.project1mutsasns.service.UserDetailServiceImpl;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Date;


@Component
@Slf4j
public class JwtTokenProvider {

    private final UserDetailServiceImpl userDetailService;
    private final long expireTimeMs;

    private String secretKey;

    public JwtTokenProvider(
            UserDetailServiceImpl userDetailService,
            @Value("${jwt.token.secret}") String secretKey) {
        this.userDetailService = userDetailService;
        this.secretKey = secretKey;
        this.expireTimeMs = 1000 * 60 * 60;
    }

    @PostConstruct
    protected void init() {
        this.secretKey = Base64.getEncoder().encodeToString(secretKey.getBytes(StandardCharsets.UTF_8)); {
        }
    }

    public String createToken(String userName, String authorities) {
        Claims claims = Jwts.claims().setSubject(userName);
        claims.put("role", authorities);

        return Jwts.builder()
                .setClaims(claims)
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + expireTimeMs))
                .signWith(SignatureAlgorithm.HS256, secretKey)
                .compact();
    }

    //토큰의 만료여부 검증
    public boolean isExpired(String token) {
        Date expiredDate = extractClaims(token).getExpiration();
        return expiredDate.before(new Date());
    }

    //토큰에 저장된 userName 반환
    public String getUserName(String token) {
        return extractClaims(token).getSubject();
    }

    //토큰 내용 추출
    private Claims extractClaims(String token) {
        return Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token).getBody();
    }

    //인증 객체 생성
    public Authentication getAuthentication(String token) {
        UserDetails userDetails = userDetailService.loadUserByUsername(getUserName(token));
        userDetails.getAuthorities().stream().forEach(userAuth -> log.info("[getAuentication Userdetails.getGrantedAuthorities = {}]", userAuth.getAuthority()));
        return new UsernamePasswordAuthenticationToken(userDetails.getUsername(), "", userDetails.getAuthorities()); //userName, Role이 담기도록 구현
    }


}
