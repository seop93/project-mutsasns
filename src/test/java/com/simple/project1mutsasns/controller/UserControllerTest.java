package com.simple.project1mutsasns.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.simple.project1mutsasns.domain.dto.UserDto;
import com.simple.project1mutsasns.domain.dto.request.UserJoinRequest;
import com.simple.project1mutsasns.domain.dto.request.UserLoginRequest;
import com.simple.project1mutsasns.exception.AppException;
import com.simple.project1mutsasns.exception.ErrorCode;
import com.simple.project1mutsasns.service.UserService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@WebMvcTest(UserControllerTest.class)
class UserControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper objectMapper;

    @MockBean
    UserService userService;


    @Test
    @DisplayName("회원가입 성공")
    @WithMockUser
    void join_success() throws Exception {

        String userName = "seop";
        String password = "1234";

        when(userService.join(any())).thenReturn(mock(UserDto.class));

        mockMvc.perform(post("/api/v1/users/join").with(csrf()).contentType(MediaType.APPLICATION_JSON).
                        content(objectMapper.writeValueAsBytes(new UserJoinRequest(userName, password)))).andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    @DisplayName("회원가입 실패 : userName 중복")
    @WithMockUser
    void join_fail() throws Exception {

        String userName = "seop";
        String password = "1234";

        when(userService.join(any()))
                .thenThrow(new AppException(ErrorCode.DUPLICATED_USER_NAME, "userName이 중복됩니다."));

        mockMvc.perform(post("/api/v1/users/join")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(new UserJoinRequest(userName, password))))
                .andDo(print())
                .andExpect(status().isConflict());
    }

    @Test
    @DisplayName("로그인 성공")
    @WithMockUser
    void login_success() throws Exception {
        String userName = "seop";
        String password = "1234";

        mockMvc.perform(post("/api/v1/users/login")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(new UserLoginRequest(userName, password))))
                .andDo(print())
                .andExpect(status().isOk());

    }


    @Test
    @DisplayName("로그인 실패 : userName 없음")
    @WithMockUser
    void login_fail_USERNAME_NOTFOUND() throws Exception {
        String userName = "seop";
        String password = "1234";

        when(userService.login(any()))
                .thenThrow(new AppException(ErrorCode.USERNAME_NOT_FOUND, "존재하지 않은 userName 입니다."));

        mockMvc.perform(post("/api/v1/users/login")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(new UserLoginRequest(userName, password))))
                .andDo(print())
                .andExpect(status().isNotFound());

    }


    @Test
    @DisplayName("로그인 실패 - password 틀림")
    @WithMockUser
    void login_fail_INVAILD_PASSWORD() throws Exception {
        String userName = "seop";
        String password = "1234";

        when(userService.login(any()))
                .thenThrow(new AppException(ErrorCode.INVALID_PASSWORD, "비밀번호가 일치하지않습니다."));

        mockMvc.perform(post("/api/v1/users/login")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(new UserLoginRequest(userName, password))))
                .andDo(print())
                .andExpect(status().isUnauthorized());

    }


}