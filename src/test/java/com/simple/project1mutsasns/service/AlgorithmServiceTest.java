package com.simple.project1mutsasns.service;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

// Annotation 없이 만든다
class AlgorithmServiceTest {

    //Spring을 안쓰고 테스트 하기 때문에 new 를 이용해 초기화 한다.
    AlgorithmService algorithmService = new AlgorithmService();
    //Pojo방식을 최대한 활용한다


    @Test
    @DisplayName("자릿수 합 잘 구하는지")
    void sumOfDigit() {
        assertEquals(21, algorithmService.sumOfDigits(687));
        assertEquals(22, algorithmService.sumOfDigits(787));

    }

}